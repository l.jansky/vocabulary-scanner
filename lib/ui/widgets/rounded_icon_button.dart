import 'package:flutter/material.dart';

class RoundedIconButton extends StatelessWidget {
  const RoundedIconButton({
    Key? key,
    required this.onTap,
    required this.icon,
    this.onLongTap,
    this.color,
  }) : super(key: key);

  final Future<void> Function() onTap;
  final Future<void> Function()? onLongTap;
  final IconData icon;
  final Color? color;

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      child: Icon(icon),
      onPressed: onTap,
      onLongPress: onLongTap,
      style: ElevatedButton.styleFrom(
        shape: const CircleBorder(),
        primary: color ?? Colors.blue, // <-- Button color
        onPrimary: Colors.white, // <-- Splash color
      ),
    );
  }
}
