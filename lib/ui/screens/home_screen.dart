import 'package:flutter/material.dart';
import 'package:vocabulary_scanner/repositories/sync_repository.dart';
import 'package:vocabulary_scanner/services/vocabulary_database.dart';
import 'package:vocabulary_scanner/setup.dart';
import 'package:vocabulary_scanner/ui/widgets/auth_login.dart';

class HomeScreen extends StatelessWidget {
  HomeScreen({Key? key}) : super(key: key);

  final _syncRepository = registry.get<SyncRepository>();
  final _numberOfWordsStream = registry.get<VocabularyDatabase>().numberOfWordsStream;

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          title: const Text('Vocabulary App'),
        ),
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              AuthLogin(
                authenticatedChild: ElevatedButton(
                  onPressed: () async {
                    ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
                      content: Text('Syncing vocabulary started'),
                    ));
                    await _syncRepository.sync();
                    ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
                      content: Text('Sync complete'),
                    ));
                  },
                  child: const Text('Sync Data'),
                ),
              ),
              StreamBuilder(
                stream: _numberOfWordsStream,
                builder: (context, snapshot) {
                  return Text('Number of words: ${snapshot.data}');
                },
              ),
              InkWell(
                onTap: () async {
                  Navigator.pushNamed(context, '/camera');
                },
                child: Stack(
                  alignment: Alignment.center,
                  children: const [
                    Icon(Icons.circle, color: Colors.grey, size: 180),
                    Icon(Icons.circle, color: Colors.white, size: 165),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
