import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:vocabulary_scanner/repositories/vocabulary_repository.dart';
import 'package:vocabulary_scanner/services/translator_service.dart';
import 'package:vocabulary_scanner/setup.dart';
import 'package:vocabulary_scanner/ui/widgets/rounded_icon_button.dart';
import 'package:wakelock/wakelock.dart';

class ResultsScreenArgs {
  ResultsScreenArgs(this.results, this.allWordsCount, this.ignoredWordsCount);
  final Map<String, String> results;
  final int allWordsCount;
  final int ignoredWordsCount;
}

class ResultsScreen extends HookWidget {
  ResultsScreen({Key? key}) : super(key: key);

  final vocabularyRepository = registry.get<VocabularyRepository>();
  final translatorService = registry.get<TranslatorService>();

  @override
  Widget build(BuildContext context) {
    Wakelock.enable();
    final args = ModalRoute.of(context)!.settings.arguments as ResultsScreenArgs;
    final initialResults = args.results;

    final results = useState(initialResults);
    final showAll = useState(false);

    const headerTextStyle = TextStyle(fontSize: 13);

    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          title: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                'All words: ${args.allWordsCount}',
                style: headerTextStyle,
              ),
              Text(
                'Ignored words: ${args.ignoredWordsCount}',
                style: headerTextStyle,
              ),
              Text(
                'New words: ${initialResults.length}',
                style: headerTextStyle,
              ),
            ],
          ),
          actions: [
            Padding(
              padding: const EdgeInsets.only(right: 40.0),
              child: GestureDetector(
                onTap: () {
                  Navigator.pushReplacementNamed(context, '/camera');
                },
                child: const Icon(Icons.search),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(right: 35.0),
              child: GestureDetector(
                onTap: () {
                  showAll.value = !showAll.value;
                },
                child: const Icon(Icons.question_mark),
              ),
            ),
          ],
        ),
        body: ListView.separated(
          separatorBuilder: (_, __) => const Divider(),
          itemCount: results.value.entries.length,
          itemBuilder: ((context, index) {
            final entry = results.value.entries.elementAt(index);
            final word = entry.key;
            final translation = entry.value;

            return ListTile(
              title: Text(word),
              trailing: Row(
                mainAxisSize: MainAxisSize.min,
                children: showAll.value
                    ? [
                        Text(
                          translation,
                          style: const TextStyle(fontSize: 16),
                        ),
                        RoundedIconButton(
                          icon: Icons.question_mark,
                          onTap: () async {
                            await translatorService.launchGoogleTranslator(word);
                          },
                        ),
                      ]
                    : [
                        RoundedIconButton(
                          icon: Icons.check,
                          onTap: () async {
                            await vocabularyRepository.setAsKnown(word);
                            final entries = results.value.entries.where((e) => e.key != word);
                            results.value = Map.fromEntries(entries);
                          },
                          color: Colors.green,
                        ),
                        RoundedIconButton(
                          icon: Icons.question_mark,
                          onLongTap: () async {
                            await translatorService.launchGoogleTranslator(word);
                          },
                          onTap: () async {
                            showModalBottomSheet<void>(
                              context: context,
                              builder: (context) {
                                return SizedBox(
                                  height: 200,
                                  child: Center(
                                    child: Column(
                                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                      mainAxisSize: MainAxisSize.min,
                                      children: <Widget>[
                                        Text(
                                          word,
                                          style: const TextStyle(fontSize: 16),
                                        ),
                                        Text(
                                          translation,
                                          style: const TextStyle(fontSize: 32),
                                        ),
                                        const SizedBox(
                                          height: 20,
                                        ),
                                        ElevatedButton(
                                          child: const Icon(Icons.close),
                                          onPressed: () => Navigator.pop(context),
                                          style: ElevatedButton.styleFrom(
                                            shape: const CircleBorder(),
                                            primary: Colors.blue, // <-- Button color
                                            onPrimary: Colors.white, // <-- Splash color
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                );
                              },
                            );
                          },
                        ),
                      ],
              ),
            );
          }),
        ),
      ),
    );
  }
}
