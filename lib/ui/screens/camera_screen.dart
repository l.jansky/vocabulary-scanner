import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:vocabulary_scanner/repositories/vocabulary_repository.dart';
import 'package:vocabulary_scanner/services/camera_service.dart';
import 'package:vocabulary_scanner/setup.dart';
import 'package:vocabulary_scanner/ui/screens/result_screen.dart';

class CameraScreen extends HookWidget {
  final cameraService = registry.get<CameraService>();
  final vocabularyRepository = registry.get<VocabularyRepository>();

  CameraScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    useEffect(() {
      cameraService
          .initCameraController()
          .then((_) => Future.delayed(const Duration(seconds: 1)))
          .then((value) => vocabularyRepository.scan())
          .then((scanResult) => Navigator.pushReplacementNamed(context, '/results',
              arguments: ResultsScreenArgs(
                scanResult.unknownWords,
                scanResult.allWordsCount,
                scanResult.ignoredWordsCount,
              )));

      return () {
        cameraService.disposeCameraController();
      };
    }, []);

    return Scaffold(
      body: StreamBuilder<bool>(
        stream: cameraService.cameraIsAvailableStream,
        builder: (context, isAvailableSnapshot) {
          final isAvailable = isAvailableSnapshot.data ?? false;
          return isAvailable
              ? Column(
                  children: [
                    AspectRatio(
                      aspectRatio: 1 / cameraService.aspectRatio,
                      child: cameraService.buildPreview(),
                    ),
                    StreamBuilder<double>(
                      stream: vocabularyRepository.scanProgressStream,
                      builder: (context, progressSnapshot) {
                        final progress = progressSnapshot.data ?? 0.0;
                        return Visibility(
                          visible: progress > 0 && progress < 1,
                          child: LinearProgressIndicator(
                            value: progress,
                            color: Colors.red,
                          ),
                        );
                      },
                    ),
                    StreamBuilder<double>(
                      stream: vocabularyRepository.translateProgressStream,
                      builder: (context, progressSnapshot) {
                        final progress = progressSnapshot.data ?? 0.0;
                        return Visibility(
                          visible: progress > 0 && progress < 1,
                          child: LinearProgressIndicator(
                            value: progress,
                            color: Colors.green,
                          ),
                        );
                      },
                    ),
                  ],
                )
              : const Center(
                  child: Text('Camera is not available'),
                );
        },
      ),
    );
  }
}
