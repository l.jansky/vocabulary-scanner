import 'package:firebase_storage/firebase_storage.dart';
import 'package:get_it/get_it.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:vocabulary_scanner/repositories/sync_repository.dart';
import 'package:vocabulary_scanner/repositories/vocabulary_repository.dart';
import 'package:vocabulary_scanner/services/camera_service.dart';
import 'package:vocabulary_scanner/services/recognizer_service.dart';
import 'package:vocabulary_scanner/services/sync_service.dart';
import 'package:vocabulary_scanner/services/translator_service.dart';
import 'package:vocabulary_scanner/services/vocabulary_database.dart';

final registry = GetIt.instance;

Future<void> setup() async {
  await Hive.initFlutter();
  final box = await Hive.openLazyBox<bool>('vocabulary');
  final vocabularyDatabase = VocabularyDatabase(box);
  registry.registerSingleton<VocabularyDatabase>(vocabularyDatabase);

  final cameraService = CameraService();
  await cameraService.init();
  registry.registerSingleton<CameraService>(cameraService);

  final recognizerService = RecognizerService();
  registry.registerSingleton<RecognizerService>(recognizerService);

  final translatorService = TranslatorService();
  registry.registerSingleton<TranslatorService>(translatorService);

  final vocabularyRepository = VocabularyRepository(
    vocabularyDatabase: vocabularyDatabase,
    cameraService: cameraService,
    recognizerService: recognizerService,
    translatorService: translatorService,
  );

  registry.registerSingleton<VocabularyRepository>(vocabularyRepository);

  final syncService = SyncService(FirebaseStorage.instance);

  final syncRepository = SyncRepository(
    vocabularyDatabase: vocabularyDatabase,
    syncService: syncService,
  );

  registry.registerSingleton<SyncRepository>(syncRepository);
}
