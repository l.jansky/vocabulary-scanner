import 'package:vocabulary_scanner/services/sync_service.dart';
import 'package:vocabulary_scanner/services/vocabulary_database.dart';

class SyncRepository {
  SyncRepository({
    required SyncService syncService,
    required VocabularyDatabase vocabularyDatabase,
  })  : _syncService = syncService,
        _vocabularyDatabase = vocabularyDatabase;

  final SyncService _syncService;
  final VocabularyDatabase _vocabularyDatabase;

  Future<void> sync() async {
    final words = await _vocabularyDatabase.getKnownWords();
    await _syncService.sync(words);
  }
}
