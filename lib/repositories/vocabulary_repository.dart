import 'dart:async';
import 'dart:io';

import 'package:camera/camera.dart';
import 'package:vocabulary_scanner/services/camera_service.dart';
import 'package:vocabulary_scanner/services/recognizer_service.dart';
import 'package:vocabulary_scanner/services/translator_service.dart';
import 'package:vocabulary_scanner/services/vocabulary_database.dart';

class ScanResult {
  final Map<String, String> unknownWords;
  final int allWordsCount;
  final int ignoredWordsCount;

  ScanResult({
    required this.unknownWords,
    required this.allWordsCount,
    required this.ignoredWordsCount,
  });
}

class VocabularyRepository {
  VocabularyRepository({
    required VocabularyDatabase vocabularyDatabase,
    required CameraService cameraService,
    required RecognizerService recognizerService,
    required TranslatorService translatorService,
  })  : _cameraService = cameraService,
        _vocabularyDatabase = vocabularyDatabase,
        _recognizerService = recognizerService,
        _translatorService = translatorService;

  final VocabularyDatabase _vocabularyDatabase;
  final CameraService _cameraService;
  final RecognizerService _recognizerService;
  final TranslatorService _translatorService;
  final StreamController<double> _scanProgressController = StreamController.broadcast();
  final StreamController<double> _translateProgressController = StreamController.broadcast();

  num _scanProgress = 0.0;
  num _translateProgress = 0.0;

  Stream<double> get scanProgressStream => _scanProgressController.stream;
  Stream<double> get translateProgressStream => _translateProgressController.stream;

  void _updateScanProgressStream(num value, {bool reset = false}) {
    if (reset) {
      _scanProgress = value;
    } else {
      _scanProgress += value;
    }
    _scanProgressController.add(_scanProgress.toDouble());
  }

  void _updateTranslateProgressStream(num value, {bool reset = false}) {
    if (reset) {
      _translateProgress = value;
    } else {
      _translateProgress += value;
    }
    _translateProgressController.add(_translateProgress.toDouble());
  }

  Future<ScanResult> scan() async {
    _updateScanProgressStream(0, reset: true);
    _updateTranslateProgressStream(0, reset: true);

    await _cameraService.setFlashLight(enabled: true);

    final unknownWords = <String, String>{};

    final rawImages = <XFile>[];
    final words = <String, int>{};

    const numberOfPictures = 5;
    const scanProgressPart = 1 / numberOfPictures;

    for (var i = 0; i < numberOfPictures; i++) {
      final rawImage = await _cameraService.takePicture();
      if (rawImage != null) {
        rawImages.add(rawImage);
        final imageWords = await _recognizerService.getWordsFromImage(rawImage.path);
        for (final word in imageWords) {
          if (words.containsKey(word)) {
            words[word] = words[word]! + 1;
          } else {
            words[word] = 1;
          }
        }

        final pictureFile = File(rawImage.path);
        try {
          print('Deleting file ${rawImage.path}');
          await pictureFile.delete();
        } catch (e) {
          print('Error during file delete');
        }
      }

      _updateScanProgressStream(scanProgressPart);
    }

    await _cameraService.setFlashLight(enabled: false);

    final translateProgressPart = words.isNotEmpty ? 1 / words.length : 0;
    words.removeWhere((key, value) => value < numberOfPictures / 2);
    var ignoredWordsCount = 0;

    for (final word in words.keys) {
      final rawWord = await _getRawWord(word);
      if (!unknownWords.containsKey(rawWord) && !_vocabularyDatabase.hasWord(rawWord)) {
        final translated = await _translatorService.translate(rawWord);
        if (translated.isNotEmpty && translated != rawWord) {
          unknownWords[rawWord] = translated;
        } else {
          ignoredWordsCount++;
        }
      }
      _updateTranslateProgressStream(translateProgressPart);
    }

    final sortedWords = unknownWords.entries.toList();
    sortedWords.sort((a, b) => a.key.toLowerCase().compareTo(b.key.toLowerCase()));

    _updateScanProgressStream(1, reset: true);
    _updateTranslateProgressStream(1, reset: true);

    return ScanResult(
      unknownWords: Map.fromEntries(sortedWords),
      allWordsCount: words.length,
      ignoredWordsCount: ignoredWordsCount,
    );
  }

  Future<void> setAsKnown(String word) async {
    await _vocabularyDatabase.saveWord(word, true);
  }

  Future<String> _getRawWord(String word) async {
    const postfixes = ['ing', 's', 'es', 'd', 'ed', 'ly'];

    for (final postfix in postfixes) {
      if (word.endsWith(postfix)) {
        final rawWord = word.substring(0, word.length - postfix.length);
        final rawWordE = '${rawWord}e';
        final rawTranslation = await _translatorService.translate(rawWord);
        final rawTranslationE = await _translatorService.translate(rawWordE);
        final originalTranslation = await _translatorService.translate(word);

        //print('TRANSLATING: $word -> $rawWord -> $rawTranslation -> $originalTranslation');

        if (rawTranslation != rawWord && _areSimilar(rawTranslation, originalTranslation)) {
          return rawWord;
        }

        if (rawTranslationE != rawWordE && _areSimilar(rawTranslationE, originalTranslation)) {
          return rawWordE;
        }
      }
    }

    return word;
  }

  bool _areSimilar(String word1, String word2) {
    const threshold = 3;

    return word1.length >= threshold &&
        word2.length > threshold &&
        word1.substring(0, threshold) == word2.substring(0, threshold);
  }
}
