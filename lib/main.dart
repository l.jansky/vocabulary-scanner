import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:vocabulary_scanner/setup.dart';
import 'package:vocabulary_scanner/ui/screens/camera_screen.dart';
import 'package:vocabulary_scanner/ui/screens/home_screen.dart';
import 'package:vocabulary_scanner/ui/screens/result_screen.dart';

import 'firebase_options.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();

  await Firebase.initializeApp(
    options: DefaultFirebaseOptions.currentPlatform,
  );

  await setup();

  runApp(const VocabularyApp());
}

class VocabularyApp extends StatelessWidget {
  const VocabularyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: HomeScreen(),
      routes: {
        '/camera': (_) => CameraScreen(),
        '/results': (_) => ResultsScreen(),
      },
    );
  }
}
