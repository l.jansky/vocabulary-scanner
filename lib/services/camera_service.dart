import 'dart:async';

import 'package:camera/camera.dart';
import 'package:flutter/material.dart';

class CameraService {
  List<CameraDescription> _cameras = [];
  CameraController? _cameraController;
  final StreamController<bool> _cameraIsAvailableController = StreamController.broadcast();
  Stream<bool> get cameraIsAvailableStream => _cameraIsAvailableController.stream;

  Future<void> init() async {
    print('Getting available cameras');
    _cameras = await availableCameras();
  }

  Future<void> initCameraController() async {
    print('Initializing camera controller');
    _cameraIsAvailableController.add(false);
    await _cameraController?.dispose();

    if (_cameras.isNotEmpty) {
      final cameraDescription = _cameras.first;
      _cameraController = CameraController(
        cameraDescription,
        ResolutionPreset.high,
        imageFormatGroup: ImageFormatGroup.jpeg,
      );

      try {
        await _cameraController!.initialize();
        _cameraIsAvailableController.add(true);
      } catch (e) {
        print('Error initializing camera: $e');
        _cameraIsAvailableController.add(false);
      }
    }
  }

  Future<void> disposeCameraController() async {
    print('Disposing camera controller');
    _cameraIsAvailableController.add(false);
    await _cameraController?.dispose();
  }

  Future<XFile?> takePicture() async {
    if (_cameraController == null) {
      return null;
    }

    if (_cameraController!.value.isTakingPicture) {
      print('Camera is already taking picture');
      return null;
    }

    try {
      XFile file = await _cameraController!.takePicture();
      return file;
    } on CameraException catch (e) {
      print('Error occured while taking picture: $e');
      return null;
    }
  }

  Widget buildPreview() {
    if (_cameraController != null) {
      return _cameraController!.buildPreview();
    } else {
      return const SizedBox.shrink();
    }
  }

  double get aspectRatio => _cameraController?.value.aspectRatio ?? 1;

  Future<void> setFlashLight({required bool enabled}) async {
    if (enabled) {
      await _cameraController?.setFlashMode(FlashMode.torch);
    } else {
      await _cameraController?.setFlashMode(FlashMode.off);
    }
  }
}
