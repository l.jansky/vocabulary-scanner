import 'package:hive/hive.dart';
import 'package:rxdart/rxdart.dart';

class VocabularyDatabase {
  VocabularyDatabase(this._box);
  final LazyBox<bool> _box;

  Stream<int> get numberOfWordsStream =>
      _box.watch().map((_) => _box.length).startWith(_box.length);

  Future<void> saveWord(String word, bool known) async {
    await _box.put(word, known);
  }

  bool hasWord(String word) => _box.containsKey(word);

  Future<Iterable<String>> getKnownWords() async {
    final allWords = _box.keys.whereType<String>();
    return allWords;
  }
}
