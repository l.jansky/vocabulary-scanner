import 'package:google_mlkit_text_recognition/google_mlkit_text_recognition.dart';

class RecognizerService {
  final textRecognizer = TextRecognizer();

  Future<Iterable<String>> getWordsFromImage(String path) async {
    final skipChars = ['-'];

    final inputImage = InputImage.fromFilePath(path);
    final text = await textRecognizer.processImage(inputImage);
    final elements = <String>{};
    for (final block in text.blocks) {
      for (final line in block.lines) {
        for (final element in line.elements) {
          final regexp = RegExp(r"[a-zA-Z\-]+");
          final regexResult = regexp.firstMatch(element.text.trim().toLowerCase());
          final word = regexResult?.group(0);
          if (word != null &&
              word.length > 2 &&
              !skipChars.contains(word[0]) &&
              !skipChars.contains(word[word.length - 1])) {
            elements.add(word);
          }
        }
      }
    }

    return elements;
  }
}
