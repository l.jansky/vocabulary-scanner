import 'dart:convert';

import 'package:firebase_storage/firebase_storage.dart';

class SyncService {
  SyncService(this._firebaseStorage) {
    _storageRef = _firebaseStorage.ref();
    _vocabularyRef = _storageRef.child('vocabulary.json');
  }

  final FirebaseStorage _firebaseStorage;
  late Reference _storageRef;
  late Reference _vocabularyRef;

  Future<void> sync(Iterable<String> data) async {
    const jsonEncoder = JsonEncoder();
    final jsonString = jsonEncoder.convert(data.toList());
    await _vocabularyRef.putString(jsonString);
  }
}
