import 'package:google_mlkit_translation/google_mlkit_translation.dart';
import 'package:url_launcher/url_launcher.dart';

class TranslatorService {
  final sourceLanguage = TranslateLanguage.english;
  final targetLanguage = TranslateLanguage.czech;

  late OnDeviceTranslator translator;

  TranslatorService() {
    translator = OnDeviceTranslator(sourceLanguage: sourceLanguage, targetLanguage: targetLanguage);
  }

  Future<String> translate(String word) async {
    final response = await translator.translateText(word);
    return response.toLowerCase();
  }

  Future<void> launchGoogleTranslator(String word) async {
    final url = Uri.parse(
        'https://translate.google.com/?sl=en&tl=cs&text=${Uri.encodeComponent(word)}&op=translate');
    await launchUrl(url);
  }
}
